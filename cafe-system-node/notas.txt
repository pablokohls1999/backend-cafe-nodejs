primero que nada el npm init y luego instalar las herramientas express mysql dotenv cors y demas
consultar ese save-dev nodemon
en el package json añadimos start para poder utilizar nodemon
creamos archivo .env y ponemos como primer punto el puerto 8080
en el archivo server.js ubicamos las tres primeras lineas require dotenv require http y el createServer
luego en el archivo server.js añadimos dos lineas mas const app y el server.listen
luego creamos el archivo index.js que tendrá express const cors const app, app.use cors, app.use express urlenconded, app.use express json, module.exports
creamos el archivo connection.js y requerimos mysql require dotenv y tambien createConnection
luegoen el .env seguimos con la cadena de conexion para mysql db_port, db_host, db_username, db_password, db_name
luego entramos desde mysql por comando y escribimos nuestra contraseña y crearemos la base de datos create database cafenodejs;
luego en el archivo connection.js seguimos llenando por ejemplo port: process.env.DB_PORT y asi con los otros que salen del .env
seguimos en el archivo connection.js añadimos el connection.connect, funcion flecha y un if de conectado y un else para el Error
exportamos el archivo connection.js
en index tambien añadimos el require de connection
probando la app con npm start resulta ser que el puerto 8080 esta ocupado lo he cambiado al 5000, tambien cambié de mysql a mysql2, pensé que ese era el Error
en la linea anterior las dos mysql y mysql2 hasta ahora funcionan bien, continuo con mysql1
creamos la primera tabla en el archivo table.sql, la tabla es user, ese codigo lo copiamos a la terminal de mysql
luego con el insert insertamos un user y ese codigo lo pegamos a la terminal de mysql y hacemos un select
creamos nuestra primera carpeta llamada routes con su archibo user.js
en ese archivo user.js requerimos express y lo exportamos con el module.exports
ahora en el index requerimos el userRoute y tambien mas abajo lo usamos app.use 'user', userRoute
vemos que hay un error porque router no está definido, lo añadimos en ese nuevo archivo de user.js añadimos la connection y abajo el express.Router
en el archivo user crearemos nuestra primer ruta que será de tipo post /signup con el request y response y su funcion flecha
despues de crear la ruta copiaremos las siguientes 20 lineas de codigo desde el req.body hasta la llave que se encuentra solita
y ahora utilizaremos postman o tambien tengo insomnia, primero utilizaré insomnia
2do dia
instalaremos jsonwebtoken y nodemailer
crearemos ruta de post login en el archivo user.js en routes
en este caso crearemos casi parecido a lo que se hizo arriba, con algunas modificaciones, atencion a las siguientes lineas
luego tambien requerimos jsonwebtoken y el dotenv con su config()
luego en la terminal ponemos node y copiamos lo siguiente require('crypto').randomBytes(64).toString('hex')
ahora en el archivo env añadimos el ACCES_TOKEN mas la contraseña rara que salió del codigo anterior en la terminal
luego nos vamos al postman y probamos la nueva ruta que es la de login
tengo que probar bien estaba probando con otra base de datos, es decir sacando el admin y password de esa base de datos, por eso no me salía
para laa ruta del login solo necesitamos el email y el password
luego nos vamos a la pagina jwt.io y pegamos ese token del admin
consultar bien sobre esta pagina del jwt
requeriremos el nodemailer en el archivo user.js dentro de la carpeta routes
en el archivo .env escribiremos el email con el password, algo asi email = youremail@gmail.com y password = yourpassword
ahora antes del module.exports en el mismo archivo user crearemos una variable transporter = nodemailer.createTransporter con algunos de sus atributos como: service, auth
y ahora crearemos la ruta post forgotpassword mas su funcion flecha y la variable user = req.body
el codigo del forgotpassword es complejo, sobre todo en la linea del html, mucho cuidado con esto, ahora probaremos con postman la ruta del forgotpassword
consultar sobre la herramienta de trabajo mailinator, porque al parecer la voy a necesitar
ahora crearemos una nueva ruta esta vez de tipo get y get con su req y res y funcion flecha
esta ruta get ha sido la menos compleja en cuanto a codigo
ahora crearemos la ruta patch con su url update
tambien al parecer no es tan compleja la ruta del update  update user set status=?;
tambien hacemos otra ruta get con el checktoken mas req y res y la funcion flecha
creo solamente la primer linea de una ruta post que la url es changePassword
luego me voy al postman y pruebo la ruta get  get  la cual es asi localhost:5000/user/get
ahora creamos nuestra segunda carpeta que es service y contendrá por ahora dos archivos authentication.js y checkRole.js
en el authentication.js requerimos dotenv y el jwt
tambien crearemos una funcion normal con su req, res, next
luego de hacer la funcion authenticateToken exportamos con las llaves ya que es una funcion, creo que no es necesario ponerlo dos veces como en el video, lo probaré
ahora en el archivo checkRole.js requerimos dotenv y hacemos una funcion normal
en el archivo .env añadimos el USER = user y esto lo tomamos en cuenta para el checkRole.js
luego ya sabemos exportamos con llave esa funcion que se encuentra en checkRole.js
luego en el user.js dentro de routes requerimos los dos servicios algo asi es const auth = require('../services/authentication')
dentro de server.js añadimos primeramente en la ruta get get el   , auth.authenticateToken
en el update y en el checkToken tambien añadimos el , auth.authenticateToken
luego probaremos en el postman la ruta get get y vemos que no nos sale autorizacion
para ello en el postman primero nos logeamos como el administrador copiamos ese token el headers, ponemos autorizacion y a lado tenemos para pegar nuestro token, recordar que es get
luego de esto a lado de headers, esto en postman clickear la autorization de tipo bearer token y enviamos, esto tambien get y nos sale forbiden
para que nos salga los usuarios debemos loguearnos con el admin y luego copiamos y pegamos el token en la autorization, esto dentro de postman obviamente
3er dia y 4to dia
en la ruta get get añadiremos el checkRole, luego probamos en el postman con el correo que hemos creado en ethereal.email
hay que recalcar que no pude utilizar mailinator, pero si ethereal.email   https://ethereal.email/create
y creé un user desde table.sql con el correo y contraseña que proporciona ethereal.email, igual verificar en el archivo table.sql
ya en postman nos logueamos con esa cuenta y luego en get authorization verificamos, creo que no me sale del todo bien, pero hay que ver, voy a seguir
luego tambien ponemos el checkRole en la ruta patch del update
luego seguimos con la ruta changePassword, algo compleja con dos query, el primero para seleccionar select * from user where email=?.....
el segundo query update user set password=?....
luego probamos wn wl postman primero nos logueamos como administrador y luego probamos la ruta del update, recordar esta es patch no es post cambiamos el status a un usuarios, se necesita el id
en la misma ruta del postman la del patch solamente la vamos a cambiar a post y poner la ruta del changePassword, es porque ahí ya tenemos la autorizacion
nos sal eun error ya que no hemos puesto el authenticateToken en changePassword, no olvidarse ponerlo
luego de nuevo cambiamos la misma ruta del postman que está en changePassword, la cambiamos ahora a checkToken con su ruta get
ahora crearemos una nueva tabla categoria con su id, name y un primary key(id)
recordar que describe category sirve para ver esa tabla lo que tiene
crearemos una nueva ruta que es la de categoria.js dentro de routes
dentro de category.js requeriremos express, la coneccion desde connection.js, el router express.Router y la carpeta servicios
crearemos la ruta post add con su authenticate y checkrole y el req, res, next 
crearemos tambien la ruta get get con su authenticateToken y el req, res, next, en el query irá select *from category order by name;
crearemos la ruta patch del update, este si con authenticatetolen y el check role
luego lo exportamos con el module.exports = router
luego en el index principal requeriremos categoryRoutes y abajo la ruta tambien 
ahora probaremos en postman tenemos que loguearnos con admin con la nueva contraseña por cierto y de ahí probar la ruta category/add y añadir el nuevo producto con la autorizacion
se probará con el usuario kathya, recibe el token, bien pero no puede agregar un producto y por ahora me parece bien, añadimos un producto volviendo a poner el token del admin
tambien probaremos la ruta categoria/get, lo que ahora entiendo es que usuario normal no puede agregarproducto, pero si verlos.
probaremos tambien el actualizar el prodcuto, necesitamos el id y el nuevo nombre la ruta es patch category/update y podemos ver de nuevo con el get  o sino desde mysql
crearemos la tabla producto y esta tiene algo importante que se va avincular con la categoria categoryId y tiene el precio price
crearemos la nueva ruta product.js
requeriremos las mismas herramientas que hicimos con la categoria express connection router auth y checkRole
creamos la primera ruta post add con su authenticate y check role y su req, res =>{}
ahora crearemos la ruta get get con su authenticate, esta ruta si ha sido dificil, seleccionamos todos los atributos del producto y el id y el name de categoria.
seguimos con el get y utilizamos INNER JOIN, la primer letra de cada atributo, tambien el as, complejo pero interesante
y ahora le exportamos como ya sabemos y tambien en index lo requerimos ya sabemos, dos veces
probamos la ruta post add, tenemos que loguearnos con el admin, hacemos dos pruebas, probamos tambien el get get, siempre con el admin, por ahora es asi
ahora crearemos una ruta interesante para el producto /getByCategory/:id para ver solamente un producto y no todos
esta ruta get de id ya no va con el req.body sino req.params.id, supongo que esto se hace para los id
5to dia 
ahora haremos la ruta /getById/:id que es un get con req,res,next, esta solamente va con el auth
crearemos la ruta patch del update que va con el auth y con el checkRole
ahora crearemos la ruta delete delete nueva para mi en este proyecto en el query ponemos delete from product where id=?
probamos la ruta getCategory que es un get
probamos tambien la ruta del product/update que es patch y la ruta del delete con su id que queremos borrar, probar con otros id incorrectos
ahora se crea una ruta muy interesante que es patch updateStatus, que es para actualizar el estado del producto, creo yo si se encuentra disponible o no
y probamos esa ruta que es patch y necesitamos el id y el status, esto en postman 
ahora instalaremos otras herramientas con npm i ejs html-pdf uuid path
despues crearemos una nueva tabla la table bill=factura que va a tener, id, uuid, name, email, contactNumber, paymentMethod, total, productDetails, createdBy y el primarykey(id)
y lo pegamos a mysql
ahora crearemos en routes nuestro primer archivo report.ejs
en ese report ejs añadimos un html 5, una etiqueta style
dentro de style ponemos table{ la cual va a tener fon family border-collapse y un width}
tambien dentro de style ponemos td, th { la cual va a tener border, text-align, padding}
ahora dentro del body añadimos un h3 con style text align 
creamos una table, luego un tr, y dentro de tr un th aqui va a ser codigo de ejs, llamamos los atributos del bill, por ejemplo, Name: <%= name%> asi con los demas
luego lo del productdetails vamos viendo en el archivo report.ejs, no es tan complejo pero van detalles extras de los que conocia, y va lo mismo table tr th
es necesario revisar el reports.ejs porque hay cosas algo complejas con las etiquetas ejs
6to dia
crearemos nueva ruta la ruta del bill
vamos a requerir express, connection, router, path, uuid, html-pdf, fs, ejs y el auth que viene desde services
creamos nuestra primer ruta que es de tipo post /generateReport, y utilizaremos el uuid.v1()
luego tambien el req.body
esto no me es tan familiar, se muestra complejo seguimos con productDetailsReport = JSON.parse(orderDetails.productDetails);
seguimos con esta ruta de post ahora nos toca el query que es un insert
la linea del connection.query ha sido compleja, mucho ojo con esto
sigue siendo complejo desde la primera condicion con el ejs, de aqui llegamos hasta el pdf.create(data).toFile
vamos a crear un nuevo folfer llamado generated_pdf
luego seguimos con el codigo desde el toFile() muy complejo ha sido esta api
luego exportaremos router con el module.exports
luego tambien en el index lo requeriremos las dos veces que ya sabemos
probamos la ruta post generateReport ha sido complejo  tambien, es mejor tener el codigo siempre, me refiero al siguiente
"[{\"id\": 1, \"name\": \"black coffee\", \"price\": 99, \"total\" 99, \"category\" \"coffee\", \"quantity\": \"1\"}]"
7mo dia
vamos a copiar y pegar un nuevo reporte de factura parecido al de arriba para despues probarlo en postman con la ruta generateReport, es la siguiente
[{\"id\": 18, \"name\": \"Doppio leche\", \"price\": 120, \"total\": 125, \"category\": \"coffee\", \"quantity\": \"1\"}, {\"id\": 5, \"name\": \"rice\", \"price\": 50, \"total\": 52, \"category\": \"cereal\", \"quantity\": \"1\"}]
vamos a crear una nueva ruta que es de tipo post que es de getPdf con su funcion flecha si es que nor sirve
constinuamos y luego del req.body tenemos la siguiente linea que es pdfPath = a la ruta del pfd + orderDetails.uuid+'.pdf';
y luego seguimos con la condicion si el archivo existe o no, y continuamos con las siguientes lineas que tenemos en el bill.js
despues del else del let productDetailsReport copiamos de la ruta anterior desde el ejs.renderFile
el ultimo el else que teniamos despues de haber copiado lo reemplazamos despues del fs.existsSync, son dos linea de codigo la del res y la del fs
luego probaremos la ruta que hemos creado que es tipo post pero obtiene getPdf, el script del json está en el generate/Report, tomaremos ese uuid y lo añadimos a la nueva ruta que hemos mencionado
tenemos un error y es sobre generateUuid que lo cambiaremos por el orderDetails.uuid
seguimos teniendo otro error de servidor que no inicia, y ha sido porque en la carpeta de generate_pdf no habiamos añadido el slash , no leia nada
ahora crearemos una nueva ruta get la cual traerá todas las facturas.
luego probaremos esa ruta get Bills en el postman
ahora crearemos la ruta del delete/:id
luego probaremos en postman esa ruta del delete, recordar que tambien podemos hacer un select * from bill;
ahora si crearemos el dashboard.js en routes
requerimos lo usual express connection router auth
crearemos nuestra primer ruta de tipo get que traerá los details con el auth y el req, res, next =>{}
seguimos con el codigo del get, solo esta vez utilizaré var ya que se usa dos veces el query y me dió problemas el let
luego exportamos el dashboard.js
luego tambien lo requerimos en index.js arriba y abajo
probamos en postman esta ruta interesante porque es del dashboard, lo cual nos trae la cantida de productos, de categorias y la de facturas
ahora empezaremos con angular desde la pagina angular.io/cli instalaremos angular desde el command promt o sino mejor desde la carpeta frontend porque creamos esa carpeta al mismo nivel del proyecto



























