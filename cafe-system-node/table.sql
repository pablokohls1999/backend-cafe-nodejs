create table user(
    id int primary key AUTO_INCREMENT,
    name varchar(250),
    contactNumber varchar(20),
    email varchar(75),
    password varchar(200),
    status varchar(20),
    role varchar(20),
    unique (email)
);

insert into user(name, contactNumber, email, password, status, role) values('Administrador', '123456', 'admin@gmail.com','admin','true','admin');
insert into user(name, contactNumber, email, password, status, role) values('Simone Wisoky', '123456789', 'simone.wisoky54@ethereal.email','cBA4HHDph2vEBxBr3h','true','user');

create table category(
    id int not null AUTO_INCREMENT,
    name varchar(255) not null,
    primary key(id)
);

delete from user where name = 'pablo';

create table product(
    id int not null AUTO_INCREMENT,
    name varchar(255) not null,
    categoryId integer NOT NULL,
    description varchar(250),
    price integer,
    status varchar(20),
    primary key(id)
);

create table bill(
    id int not null AUTO_INCREMENT,
    uuid varchar(200) not null,
    name varchar(255) not null,
    email varchar(255) not null,
    contactNumber varchar(20) not null,
    paymentMethod varchar(50) not null,
    total int not null,
    productDetails JSON default null,
    createdBy varchar(255) not null,
    primary key(id)
);